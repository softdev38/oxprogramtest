/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Project/Maven2/JavaApp/src/main/java/${packagePath}/${mainClassName}.java to edit this template
 */

package com.mycompany.ox;

/**
 *
 * @author natthawadee
 */
public class OX {

     public static void main(String[] args) {
        Game game = new Game();
        game.showWelcome();
        game.newBoard();

        while (true) {
            game.showTable();
            game.showTurn();
            game.inputRowCol();
            if (game.isFinish()) {
                game.showTable();
                game.showResult();
                game.showStat();
                if (game.isContinue()) {
                    game.newBoard();
                }else{
                    System.out.println("Bye Bye.");
                    break;
                }
            }
        }

    }
}
