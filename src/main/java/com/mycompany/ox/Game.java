/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.ox;

import java.util.Scanner;

/**
 *
 * @author natthawadee
 */
public class Game {
    private player o;
    private player x;
    private int row;
    private int col;
    private Board board;
    Scanner kb = new Scanner(System.in);

    public Game() {
        this.o = new player('O');
        this.x = new player('X');
    }

    public void newBoard() {
        this.board = new Board(o, x);
    }

    public void showWelcome() {
        System.out.println("Welcome to OX Game");
    }

    public void showTable() {
        char[][] table = this.board.getTable();
        for (int i = 0; i < table.length; i++) {
            for (int j = 0; j < table.length; j++) {
                System.out.print(table[i][j] + " ");
            }
            System.out.println();
        }
    }

    public void showTurn() {
        player player = board.getCurrentPlayer();
        System.out.println("Turn " + player.getSymbol());
    }

    public void inputRowCol() {
        while (true) {
            System.out.println("Please input row, col :");
            row = kb.nextInt();
            col = kb.nextInt();
            if (board.setRowCol(row, col)) {
                return;
            }
        }
    }

    public boolean isFinish() {
        if (board.isDraw() || board.isWin()) {
            return true;
        }
        return false;
    }
    
    public void showStat(){
        System.out.println(o);
        System.out.println(x);
    }
    
    public boolean isContinue(){
        System.out.println("Continue this game? (Y/N): " );
        char con = kb.next().charAt(0);
        if(con=='Y'||con=='y'){
            return true;
        }else{
            return false;
        }
    }
            
    public void showResult(){
        if(board.isDraw()){
            System.out.println("Draw!!!");
        }else if(board.isWin()){
            System.out.println(">>>"+board.getCurrentPlayer().getSymbol()+" Win<<<");
        }
    }
}
